/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.inheritance;

/**
 *
 * @author ASUS
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani","White",0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang","black&White");
        dang.speak();
        dang.walk();
        
        Dog to = new Dog("To","Brown");
        dang.speak();
        dang.walk();
        
        Dog mome = new Dog("Mome","black&White");
        dang.speak();
        dang.walk();
        
        Dog bat = new Dog("Bat","black&White");
        dang.speak();
        dang.walk();
        
        Cat zero = new Cat("Zero","Orange");
        zero.speak();
        zero.walk();
        
        Duck zom = new Duck("Zom","Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        
        Duck gabgab = new Duck("GabGab","Black");
        zom.speak();
        zom.walk();
        zom.fly();
        
        
        
        System.out.println("Zom is Anumal: "+(zom instanceof Animal));
        System.out.println("Zom is Duck: "+(zom instanceof Duck));
        System.out.println("Zom is cat: "+(zom instanceof Object));
        System.out.println("Animal is Dog: "+(animal instanceof Dog));
        System.out.println("Zom is Anumal: "+(animal instanceof Animal));
        
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = zom;
        ani2 = zero;
        
        System.out.println("Ani1: zom is Duck "+ (ani1 instanceof Duck));
        
        Animal[] animals = {dang, zero ,zom ,to ,mome, bat ,gabgab};
        for(int i=0;i<animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
        }
    }
}
